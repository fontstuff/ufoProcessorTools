import os
from zipfile import ZipFile

import ufoProcessor
from fontTools.misc import plistlib
from ufoProcessor import UFOProcessorError

__version__ = "0.0.1"

def getUFOVersion(ufoPath):
    """
    Return formatVersion from UFO, may be UFO package (folder) 
    or single file (zip).
    """
    if os.path.isdir(ufoPath):
        return ufoProcessor.getUFOVersion(ufoPath)
    metainfoFileName = "metainfo.plist"
    if os.path.isfile(ufoPath):
        with ZipFile(ufoPath, "r") as ufoZip:
            for name in ufoZip.namelist():
                if name.endswith(f"/{metainfoFileName}"):
                    with ufoZip.open(name, mode="r") as metainfoFile:
                        metainfo = plistlib.load(metainfoFile)
                        return metainfo.get("formatVersion")


class DesignSpaceProcessor(ufoProcessor.DesignSpaceProcessor):

    def generateUFO(self, processRules=True, glyphNames=None, pairs=None, bend=False):
        # makes the instances
        # option to execute the rules
        # make sure we're not trying to overwrite a newer UFO format
        self.loadFonts()
        self.findDefault()
        if self.default is None:
            # we need one to genenerate
            raise UFOProcessorError(
                "Can't generate UFO from this designspace: no default font.", self
            )
        for instanceDescriptor in self.instances:
            if instanceDescriptor.path is None:
                continue
            font = self.makeInstance(
                instanceDescriptor,
                processRules,
                glyphNames=glyphNames,
                pairs=pairs,
                bend=bend,
            )
            folder = os.path.dirname(os.path.abspath(instanceDescriptor.path))
            path = instanceDescriptor.path
            if not os.path.exists(folder):
                os.makedirs(folder)
            if os.path.exists(path):
                existingUFOFormatVersion = getUFOVersion(path)
                if existingUFOFormatVersion > self.ufoVersion:
                    self.problems.append(
                        "Can’t overwrite existing UFO%d with UFO%d."
                        % (existingUFOFormatVersion, self.ufoVersion)
                    )
                    continue
            font.save(path, self.ufoVersion)
            self.problems.append(
                "Generated %s as UFO%d" % (os.path.basename(path), self.ufoVersion)
            )
        return True

    def loadFonts(self, reload=False):
        # Load the fonts and find the default candidate based on the info flag
        if self._fontsLoaded and not reload:
            return
        names = set()
        for i, sourceDescriptor in enumerate(self.sources):
            if sourceDescriptor.name is None:
                # make sure it has a unique name
                sourceDescriptor.name = "master.%d" % i
            if sourceDescriptor.name not in self.fonts:
                if os.path.exists(sourceDescriptor.path):
                    self.fonts[sourceDescriptor.name] = self._instantiateFont(
                        sourceDescriptor.path
                    )
                    self.problems.append(
                        "loaded master from %s, layer %s, format %d"
                        % (
                            sourceDescriptor.path,
                            sourceDescriptor.layerName,
                            getUFOVersion(sourceDescriptor.path),
                        )
                    )
                    names |= set(self.fonts[sourceDescriptor.name].keys())
                else:
                    self.fonts[sourceDescriptor.name] = None
                    self.problems.append(
                        "source ufo not found at %s" % (sourceDescriptor.path)
                    )
        self.glyphNames = list(names)
        self._fontsLoaded = True
